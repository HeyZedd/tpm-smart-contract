
**All commands are issued from the bin folder, unless stated otherwise**

# Requirements
Before launch of the network two requirements must be met:
1) The [prerequisites tutorial](https://hyperledger-fabric.readthedocs.io/en/release-2.2/prereqs.html) from the Hyperledger Fabric documentation must be completed.
2) jq must be installed (`sudo apt-get install jq`).


# Setup network
The network consists of three organization. One orderer organization and two peer organizations. The MSP of the orderer resides in the org0 folder and the MSP of the other organizations can be found in the folders org1 and org2. These folders contain certificates to identify members of the organizations. In the following code snippet you can see an incomplete structure of a MSP folder, to highlight important files used to setup the network. 
```
├─ msp
    ├── cacerts
    │   └── <CA certificate issued by organization CA>
    ├── config.yaml 
    ├── keystore
    │   └── <private key>
    ├── signcerts
    │   └── <public key>
```
All MSP folders follow the same structure. The config.yaml file defines the organizational units of the organization, where organizations can be split up in different departments with each their own set of responsibilities.

## Launch orderer
Set the environment variables for the orderer.
```
export FABRIC_CFG_PATH=${PWD}/../config

./orderer start
```
First, the FABRIC_CFG_PATH environment variable is set to the path of folder that contains the orderer.yaml, this file defines the configuration of the orderer.
## Launch peer for org1
The following environment variables must be set, before starting the peer for org1
```
export CORE_PEER_ID=peer1-org1
export CORE_PEER_ADDRESS=localhost:7051
export CORE_PEER_LOCALMSPID=org1MSP
export CORE_PEER_MSPCONFIGPATH=${PWD}/../org1/peer1/msp
export CORE_VM_ENDPOINT=unix:///host/var/run/docker.sock
export CORE_VM_DOCKER_HOSTCONFIG_NETWORKMODE=guide_fabric-ca
export FABRIC_LOGGING_SPEC=debug
export CORE_PEER_TLS_ENABLED=true
export CORE_PEER_TLS_CERT_FILE=${PWD}/../org1/peer1/tls-msp/signcerts/cert.pem
export CORE_PEER_TLS_KEY_FILE=${PWD}/../org1/peer1/tls-msp/keystore/key.pem
export CORE_PEER_TLS_ROOTCERT_FILE=${PWD}/../org1/peer1/tls-msp/tlscacerts/tls-localhost-7052.pem
export CORE_PEER_GOSSIP_USELEADERELECTION=true
export CORE_PEER_GOSSIP_ORGLEADER=false
export CORE_PEER_GOSSIP_EXTERNALENDPOINT=localhost:7060
export CORE_PEER_FILESYSTEMPATH=/var/hyperledger/production/org1
export CORE_PEER_GOSSIP_SKIPHANDSHAKE=true
export CORE_OPERATIONS_LISTENADDRESS=localhost:9444
export FABRIC_CFG_PATH=${PWD}/../config

./peer node start
```
This peer will run on localhost port 7051.

## Launch peer for org2
The following environment variables must be set, before starting the peer for org2
```
export CORE_PEER_ID=peer1-org2
export CORE_PEER_ADDRESS=localhost:7053
export CORE_PEER_LISTENADDRESS=localhost:7053
export CORE_PEER_CHAINCODELISTENADDRESS=localhost:7054
export CORE_PEER_LOCALMSPID=org2MSP
export CORE_PEER_MSPCONFIGPATH=${PWD}/../org2/peer1/msp
export CORE_VM_ENDPOINT=unix:///host/var/run/docker.sock
export CORE_VM_DOCKER_HOSTCONFIG_NETWORKMODE=guide_fabric-ca
export FABRIC_LOGGING_SPEC=debug
export CORE_PEER_TLS_ENABLED=true
export CORE_PEER_TLS_CERT_FILE=${PWD}/../org2/peer1/tls-msp/signcerts/cert.pem
export CORE_PEER_TLS_KEY_FILE=${PWD}/../org2/peer1/tls-msp/keystore/key.pem
export CORE_PEER_TLS_ROOTCERT_FILE=${PWD}/../org2/peer1/tls-msp/tlscacerts/tls-localhost-7052.pem
export CORE_PEER_GOSSIP_USELEADERELECTION=true
export CORE_PEER_GOSSIP_ORGLEADER=false
export CORE_PEER_GOSSIP_EXTERNALENDPOINT=localhost:7058
export CORE_PEER_GOSSIP_SKIPHANDSHAKE=true
export CORE_OPERATIONS_LISTENADDRESS=localhost:9445
export FABRIC_CFG_PATH=${PWD}/../config
export CORE_PEER_FILESYSTEMPATH=/var/hyperledger/production/org2
export CORE_PEER_GOSSIP_ENDPOINT=localhost:7055
export CORE_PEER_GOSSIP_BOOTSTRAP=localhost:7055
./peer node start
```
This peer will run on localhost port 7053
## Create and join a channel
When performing certain tasks, such as creating and joining a channel. It is very important to point the environment variables to the right MSP folders, this way the right permissions are configured to perform these tasks.
### org1
Set the environment variables to act as the admin of org1 and join the channel
```
export FABRIC_CFG_PATH=${PWD}/../config
export CORE_PEER_LOCALMSPID=org1MSP
export CORE_PEER_MSPCONFIGPATH=${PWD}/../org1/admin/msp
export CORE_PEER_ADDRESS=localhost:7051
export CORE_PEER_TLS_CERT_FILE=${PWD}/../org1/peer1/tls-msp/signcerts/cert.pem
export CORE_PEER_TLS_KEY_FILE=${PWD}/../org1/peer1/tls-msp/keystore/key.pem
export CORE_PEER_TLS_ROOTCERT_FILE=${PWD}/../org1/peer1/tls-msp/tlscacerts/tls-localhost-7052.pem

./peer channel join -b ${PWD}/../channel.block
```
### org2
Set the environment variables to act as the admin of org2 and join the channel
```
export CORE_PEER_ADDRESS=localhost:7053
export CORE_PEER_ID=peer1-org2
export CORE_PEER_TLS_ENABLED=true
export CORE_PEER_LOCALMSPID=org2MSP
export FABRIC_CFG_PATH=../config
export CORE_PEER_TLS_CERT_FILE=${PWD}/../org2/peer1/tls-msp/signcerts/cert.pem
export CORE_PEER_TLS_KEY_FILE=${PWD}/../org2/peer1/tls-msp/keystore/key.pem
export CORE_PEER_TLS_ROOTCERT_FILE=${PWD}/../org2/peer1/tls-msp/tlscacerts/tls-localhost-7052.pem

./peer channel join -b ${PWD}/../channel.block
```
# Install chaincode
The chaincode package (myccpack.tgz) to install on the server only contains information about the builder that needs to build the package once installed and the address to the chaincode server running on the machine.
## org1
```
export CORE_PEER_ADDRESS=localhost:7051
export CORE_PEER_LOCALMSPID=org1MSP
export CORE_PEER_ID=peer1-org1
export CORE_PEER_TLS_ROOTCERT_FILE=${PWD}/../org1/peer1/tls-msp/tlscacerts/tls-localhost-7052.pem
export CORE_PEER_TLS_ENABLED=true
export CORE_PEER_MSPCONFIGPATH=${PWD}/../org1/admin/msp
export FABRIC_CFG_PATH=${PWD}/../config

./peer lifecycle chaincode install ../myccpack.tgz 
```
Take note of the chaincode id (ccid) returned by the install command, paste it in the chaincode-server/server.go file.

Now the chaincode can be approved. This only has be done by one organization because of the way the channel is configured.
```
./peer lifecycle chaincode approveformyorg -o localhost:7056 --channelID mychannel --name mycc --version 1.0 --package-id $CC_PACKAGE_ID --sequence 1 --tls --cafile ${PWD}/../org0/orderer/tls-msp/tlscacerts/tls-localhost-7052.pem

./peer lifecycle chaincode checkcommitreadiness -o localhost:7056 --channelID mychannel --name mycc --version 1.0 --sequence 1 --tls --cafile ${PWD}/../org0/orderer/tls-msp/tlscacerts/tls-localhost-7052.pem

./peer lifecycle chaincode commit -o localhost:7056 --channelID mychannel --name mycc --version 1.0 --sequence 1 --tls --cafile ${PWD}/../org0/orderer/tls-msp/tlscacerts/tls-localhost-7052.pem --peerAddresses localhost:7051 --tlsRootCertFiles root-certs/tls-ca-cert.pem 

./peer lifecycle chaincode querycommitted --channelID mychannel --name mycc --cafile ${PWD}/../org0/orderer/tls-msp/tlscacerts/tls-localhost-7052.pem
```
## org2
Set the environment variables to act as the admin of org2.
```
export CORE_PEER_ADDRESS=localhost:7053
export CORE_PEER_LOCALMSPID=org2MSP
export CORE_PEER_ID=peer1-org2
export CORE_PEER_TLS_ENABLED=true
export FABRIC_CFG_PATH=../config
export CORE_PEER_TLS_CERT_FILE=${PWD}/../org2/peer1/tls-msp/signcerts/cert.pem
export CORE_PEER_TLS_KEY_FILE=${PWD}/../org2/peer1/tls-msp/keystore/key.pem
export CORE_PEER_TLS_ROOTCERT_FILE=${PWD}/../org2/peer1/tls-msp/tlscacerts/tls-localhost-7052.pem

./peer lifecycle chaincode install ../myccpack.tgz 
```
The chaincode is already approved by org1, so after the chaincode server is started it is time to interact with the chaincode. 
# Run chaincode server
From the chaincode-server directory run: `go run *.go`

# Interact with chaincode
With the orderer and peers running, we can start invoking chaincode functions.
The following functions are invoked by the peer of org1, to invoke functions for the peer of org2 change the peerAddresses flag.
```
# Create an asset
./peer chaincode invoke -o localhost:7056 --tls --cafile ${PWD}/../org0/orderer/tls-msp/tlscacerts/tls-localhost-7052.pem -C mychannel -n mycc --peerAddresses localhost:7051 --tlsRootCertFiles root-certs/tls-ca-cert.pem -c '{"function":"createAsset","Args":["asset1","example description"]}'

# Read an asset from the ledger
./peer chaincode invoke -o localhost:7056 --tls --cafile ${PWD}/../org0/orderer/tls-msp/tlscacerts/tls-localhost-7052.pem -C mychannel -n mycc --peerAddresses localhost:7051 --tlsRootCertFiles root-certs/tls-ca-cert.pem -c '{"function":"readAsset","Args":["asset1"]}'

# Update the public description of an asset
./peer chaincode invoke -o localhost:7056 --tls --cafile ${PWD}/../org0/orderer/tls-msp/tlscacerts/tls-localhost-7052.pem -C mychannel -n mycc --peerAddresses localhost:7051 --tlsRootCertFiles root-certs/tls-ca-cert.pem -c '{"function":"changePublicDescription","Args":["asset1", new example description"]}'

# Transfer an asset
./peer chaincode invoke -o localhost:7056 --tls --cafile ${PWD}/../org0/orderer/tls-msp/tlscacerts/tls-localhost-7052.pem -C mychannel -n mycc --peerAddresses localhost:7051 --tlsRootCertFiles root-certs/tls-ca-cert.pem -c '{"function":"transferAsset","Args":["asset1", "org2MSP"]}'
```


