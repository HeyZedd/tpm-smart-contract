package main

import (
	"crypto"
	"crypto/ecdsa"
	"crypto/rand"
	"crypto/sha256"
	"crypto/x509"
	"encoding/asn1"
	"fmt"
	"io"
	"math/big"

	"github.com/google/go-tpm/tpm2"
	"github.com/google/go-tpm/tpmutil"
)

var (
	tmpl_rsa = tpm2.Public{
		Type:    tpm2.AlgRSA,
		NameAlg: tpm2.AlgSHA256,
		Attributes: tpm2.FlagFixedTPM | // Key can't leave the TPM
			tpm2.FlagFixedParent | // Key can't change parent
			tpm2.FlagSensitiveDataOrigin | // Key created by the TPM
			tpm2.FlagUserWithAuth | // Uses password, can be empty
			tpm2.FlagNoDA |
			tpm2.FlagRestricted | // Key uses for challenge-response protocol
			tpm2.FlagDecrypt, // Key can be used for decryption
		RSAParameters: &tpm2.RSAParams{
			Symmetric: &tpm2.SymScheme{
				Alg:     tpm2.AlgAES,
				KeyBits: 128,
				Mode:    tpm2.AlgCFB},
			KeyBits:    2048,
			ModulusRaw: make([]byte, 256), // Actual data necoded in the template
		},
	}

	tmpl_ecc = tpm2.Public{ // Change name to something more informatie
		Type:    tpm2.AlgECC,
		NameAlg: tpm2.AlgSHA256,
		Attributes: tpm2.FlagFixedTPM | // Key can't leave the TPM
			tpm2.FlagFixedParent | // Key can't change parent
			tpm2.FlagSensitiveDataOrigin | // Key created by the TPM
			tpm2.FlagUserWithAuth | // Uses password, can be empty
			tpm2.FlagSign, // Key can be used to sign data
		ECCParameters: &tpm2.ECCParams{
			Sign:    &tpm2.SigScheme{Alg: tpm2.AlgECDSA, Hash: tpm2.AlgSHA256},
			CurveID: tpm2.CurveNISTP256,
			Point: tpm2.ECPoint{
				XRaw: make([]byte, 32),
				YRaw: make([]byte, 32),
			},
		},
	}

	tpmPath_signer = "/dev/tpmrm0"
)

// Signer object that store the handle for the TPM
type signer struct {
	tpm    io.ReadWriter
	handle tpmutil.Handle
	pub    crypto.PublicKey
}

// Signed document that get stored on the shared ledger
type SignedDocument struct {
	Asset     Asset  `json:"asset"`
	PublicKey []byte `json:"pubKey"`
	Signature []byte `json:"signature"`
}

// Asset struct and properties must be exported (start with capitals) to work with contract api metadata
type Asset struct {
	ObjectType        string `json:"objectType"`
	ID                string `json:"assetID"`
	OwnerOrg          string `json:"ownerOrg"`
	PublicDescription string `json:"publicDescription"`
}

// Public returns the public key of the signer
func (s *signer) Public() crypto.PublicKey {
	return s.pub
}

// Sign signs the provided digest
func (s *signer) Sign(r io.Reader, digest []byte, opts crypto.SignerOpts) ([]byte, error) {
	password := ""	// Change this when used in production environments

	// Compute a digest using the given loaded key
	sig, err := tpm2.Sign(s.tpm, s.handle, password, digest, nil, nil)
	if err != nil {
		return nil, fmt.Errorf("can't sign the data: %v", err)
	}
	if sig.RSA != nil {		// Use RSA as signing algorithm
		return sig.RSA.Signature, nil
	}
	if sig.ECC != nil {		// Use ECC as signing algorithm
		return asn1.Marshal(struct {
			R *big.Int
			S *big.Int
		}{sig.ECC.R, sig.ECC.S})
	}
	return nil, fmt.Errorf("unsupported signature type: %v", sig.Alg)
}

// newSigner returns an signer from the TPM, that can be used to sign digests
func newSigner(tpm io.ReadWriter, handle tpmutil.Handle) (*signer, error) {
	// Read the public part of the TPM handle
	tpmPub, _, _, err := tpm2.ReadPublic(tpm, handle)
	if err != nil {
		return nil, fmt.Errorf("can't read public blob: %v", err)
	}

	// Get public key from the public area of the handle
	pub, err := tpmPub.Key()
	if err != nil {
		return nil, fmt.Errorf("can't decode the public key: %v", err)
	}
	return &signer{tpm, handle, pub}, nil
}

// signMsg signs a message
func (priv *signer) signMsg(msg []byte) (*ecdsa.PublicKey, [32]byte, []byte, error) {
	// Hash the message with sha256
	digest := sha256.Sum256(msg)

	// Sign the digest
	sig, err := priv.Sign(rand.Reader, digest[:], crypto.SHA256)
	if err != nil {
		return nil, [32]byte{}, nil, fmt.Errorf("can't sign the message: %v", err)
	}

	// Get the public key of the signer
	pub, ok := priv.Public().(*ecdsa.PublicKey)
	if !ok {
		return nil, [32]byte{}, nil, fmt.Errorf("expected ecdsa.Publickey got: %T", priv.Public())
	}

	return pub, digest, sig, nil
}

// verify verifies if the provided digest and signature are valid
func verify(digest [32]byte, pub *ecdsa.PublicKey, sig []byte) error {
	if !ecdsa.VerifyASN1(pub, digest[:], sig) {
		return fmt.Errorf("failed to verify signature")
	}
	return nil
}

func createSigner(rwc io.ReadWriter) (*signer, error) {
	/* Create Storage root key and Application key*/

	// Create Storage root key which stays inside the tpm
	parentPass, ownerPass := "", ""
	storageRootKey, _, err := tpm2.CreatePrimary(rwc, tpm2.HandleOwner, tpm2.PCRSelection{}, parentPass, ownerPass, tmpl_rsa)
	if err != nil {
		return nil, fmt.Errorf("can't create storage root key: %v", err)
	}

	// Create key pair using the storage root key
	privBlob, pubBlob, _, _, _, err := tpm2.CreateKey(rwc, storageRootKey, tpm2.PCRSelection{}, parentPass, ownerPass, tmpl_ecc)
	if err != nil {
		return nil, fmt.Errorf("can't create the applicaton key: %v", err)
	}

	// Load the key pair into an object in the TPM
	// The appKey is a handle to
	parentAuth := ""
	appKey, _, err := tpm2.Load(rwc, storageRootKey, parentAuth, pubBlob, privBlob)
	if err != nil {
		return nil, fmt.Errorf("can't load private and public blobs into the TPM: %v", err)
	}

	// Create a new signer to sign data using the application key
	priv, err := newSigner(rwc, appKey)
	if err != nil {
		fmt.Printf("can't create a new signer: %v", err)
	}

	return priv, nil
}

func (asset *Asset) assetToByteArray() []byte {
	// Returns struct values in the from of:
	// {structValue1 structValue2 ...} then converts
	// this string into a byte array
	return []byte(fmt.Sprintf("%v", asset))
}

func (asset *Asset) signAsset_onChain(rwc io.ReadWriter) (SignedDocument, error) {
	// Create signer t
	signer, err := createSigner(rwc)
	if err != nil {
		return SignedDocument{}, fmt.Errorf("something went wrong while creating the signer: %v", err)
	}

	pub, _, sig, err := signer.signMsg(asset.assetToByteArray())
	if err != nil {
		return SignedDocument{}, fmt.Errorf("something went wrong while signing the asset: %v", err)
	}

	//stackoverflow.com/questions/52502511/how-to-generate-bytes-array-from-publickey
	pubData, err := x509.MarshalPKIXPublicKey(pub)
	if err != nil {
		return SignedDocument{}, fmt.Errorf("something went wrong while marshalling the public key: %v", err)
	}

	// x,y  := elliptic.UnmarshalCompressed(, pubData)
	doc := SignedDocument{
		Asset: *asset, //TODO Add one way hash such that people can verify but not view the asset properties

		// Memory usage reduced from 24 to 16 by conversion from []byte to string
		PublicKey: pubData,
		// Curve:     pub.Curve, //Weierstrass curve
		Signature: sig,
	}

	return doc, nil
}

func (doc *SignedDocument) verifyDoc_onChain() (bool, error) {
	// Hash asset byte array
	digest := sha256.Sum256(doc.Asset.assetToByteArray())

	pubKey, err := x509.ParsePKIXPublicKey(doc.PublicKey)
	if err != nil {
		return false, err
	}

	// Verify the digital signature
	resErr := verify(digest, pubKey.(*ecdsa.PublicKey), doc.Signature)
	if resErr != nil {
		return false, err
	} else {
		return true, nil
	}
}

func GetTPM() (io.ReadWriteCloser, func() error, error) {
	rwc, err := tpm2.OpenTPM(tpmPath_signer)
	if err != nil {
		return nil, nil, fmt.Errorf("can't open tpm: %v", err)
	}
	closer := func() error { // Notify user if the TPM was not closed
		if err := rwc.Close(); err != nil {
			return fmt.Errorf("can't close TPM %q: %v", tpmPath_signer, err)
		}
		return nil
	}

	return rwc, closer, nil
}

// func main() {
// 	rwc, closer, err := getTPM()
// 	defer closer()

// 	if err != nil {
// 		fmt.Printf("%v\n", err)
// 	}

// 	asset := Asset{
// 		ObjectType:        "asset",
// 		ID:                "asset1",
// 		OwnerOrg:          "Org1MSP",
// 		PublicDescription: "A new asset for Org1MSP",
// 	}

// 	/* Signing process */
// 	doc, err := asset.signAsset_onChain(rwc)
// 	if err != nil {
// 		fmt.Printf("%v\n", err.Error())
// 	}
// 	print("Signing complete!\n")

// 	/* Storing process */
// 	res, err := json.Marshal(doc)
// 	if err != nil {
// 		print(err.Error())
// 	}
// 	fmt.Printf("%s\n\n", res)

// 	/* Getting the stored data */
// 	var unmarshal_res SignedDocument
// 	errhting := json.Unmarshal(res, &unmarshal_res)
// 	if errhting != nil {
// 		fmt.Printf("%v\n", errhting)
// 	}
// 	// fmt.Printf("%v\n", unmarshal_res)

// 	/* Verification process */
// 	doc.Asset.OwnerOrg = ""
// 	verified, err := doc.verifyDoc_onChain()
// 	if err != nil {
// 		fmt.Printf("%v\n", err)
// 	}

// 	if verified {
// 		print("Asset has not been tampered\n")
// 	} else {
// 		print("Asset has been tampered with\n")
// 	}
// }
