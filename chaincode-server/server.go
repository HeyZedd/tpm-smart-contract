package main

import (
	b64 "encoding/base64"
	"encoding/json"
	"fmt"

	"github.com/hyperledger/fabric-chaincode-go/shim"
	"github.com/hyperledger/fabric-contract-api-go/contractapi"
	pb "github.com/hyperledger/fabric-protos-go/peer"
)

type SmartContract struct {
	contractapi.Contract
}

/*
	This function must be implemented for the shim chaincode interface.

	It can be used to populate the ledger upon initialization of the
	chaincode server. This is not necessary for this particulair use case.
*/
func (s *SmartContract) Init(stub shim.ChaincodeStubInterface) pb.Response {
	return shim.Success(nil)
}

/*
	This function must be implemented for the shim chaincode interface.

	It functions as gateway to the chaincode functions.
	Whenever a peer sends a request, this is the first function
	that gets executed. The invoke exposes the createAsset,
	readAsset, transferAsset and changePublicDescription
	functions to the peers.
*/
func (s *SmartContract) Invoke(stub shim.ChaincodeStubInterface) pb.Response {

	// Get function name and input parameters provided by the client
	funcName, args := stub.GetFunctionAndParameters()
	numberOfArguments := len(args)

	// Matches the provide function name to the actual chaincode
	switch funcName {
	case "readAsset": // Calls the readAsset chaincode
		if numberOfArguments == 1 {
			doc, err := s.ReadDocument(stub, args[0])
			if err != nil {
				return shim.Error(fmt.Sprintf("failed to read asset: %v", err))
			}

			// Print the document information on the server side
			fmt.Printf("Asset info:\n   Asset ID: %v\n   Owner: %v\n   Public Key: %v\n   Signature: %v\n",
				doc.Asset.ID, doc.Asset.OwnerOrg,
				b64.StdEncoding.EncodeToString(doc.PublicKey),
				b64.StdEncoding.EncodeToString(doc.Signature),
			)
			return shim.Success(nil)
		} else {
			return shim.Error(fmt.Sprintf("expected 1 argument, but got: %v", numberOfArguments))
		}
	case "createAsset": // Calls the createAsset chaincode
		if numberOfArguments == 2 {
			return s.CreateAsset(stub, args)
		} else {
			return shim.Error(fmt.Sprintf("expected 2 arguments, but got: %v", numberOfArguments))
		}
	case "changePublicDescription": // Calls the changePublicDescription chaincode
		if numberOfArguments == 2 {
			return s.ChangePublicDescription(stub, args)
		} else {
			return shim.Error(fmt.Sprintf("expected 2 arguments, but got: %v", numberOfArguments))
		}
	case "transferAsset": // Calls the transferAsset chaincode
		if numberOfArguments == 2 {
			return s.TransferAsset(stub, args)
		} else {
			return shim.Error(fmt.Sprintf("expected 2 arguments, but got: %v", numberOfArguments))
		}
	default:
		return shim.Error(`Invalid invoke function name. Expecting "readAsset", "createAsset", "changePublicDescription" or "transferAsset"`)
	}
}

// CreateAsset creates an asset and sets the ownership to the client's organisation
func (s *SmartContract) CreateAsset(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	assetID := args[0]
	publicDescription := args[1]

	// Get the clients MSP id
	clientOrgID, err := shim.GetMSPID()
	if err != nil {
		return shim.Error(fmt.Sprintf("failed to get the the client's MSPID: %v", err))
	}

	// Create a new asset associated with the creator's organisation
	asset := Asset{
		ObjectType:        "asset",
		ID:                assetID,
		OwnerOrg:          clientOrgID,
		PublicDescription: publicDescription,
	}

	/* Begin research project part */
	// Instantiate a connection with the tpm to sign assets
	rwc, closer, err := GetTPM()
	if err != nil {
		return shim.Error(fmt.Sprintf("failed to connect to the TPM: %v", err))
	}
	defer closer() // Closes the tpm at the end of usage

	// Sign the asset using the private key stored in the TPM
	doc, err := asset.signAsset_onChain(rwc)
	if err != nil {
		return shim.Error(fmt.Sprintf("failed to sign the asset: %v", err))
	}
	/* End research project part */

	// Prepare the signed document to be stored on the shared ledger
	docBytes, err := json.Marshal(doc)
	if err != nil {
		return shim.Error(fmt.Sprintf("failed to create document JSON: %v", err))
	}

	// Store the signed document on the shared ledger
	err = stub.PutState(asset.ID, docBytes)
	if err != nil {
		return shim.Error(fmt.Sprintf("failed to write the document to the ledger : %v", err))
	}

	return shim.Success(nil)
}

// ChangePublicDescription changes the public description of an asset
func (s *SmartContract) ChangePublicDescription(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	assetID := args[0]
	newDescription := args[1]

	// Read an asset with the provided assetID from the shared ledger
	doc, err := s.ReadDocument(stub, assetID)
	if err != nil {
		return shim.Error(fmt.Sprintf("failed to get signed document: %v", err))
	}

	// Change the public description of the asset
	doc.Asset.PublicDescription = newDescription

	/* Begin research project part */
	// Instantiate a connection with the tpm to sign the updated asset
	rwc, closer, err := GetTPM()
	if err != nil {
		return shim.Error(fmt.Sprintf("failed to connect to the TPM: %v", err))
	}
	defer closer() // Closes the tpm at the end of usage

	// Sign the asset using the private key stored in the TPM
	updatedDoc, err := doc.Asset.signAsset_onChain(rwc)
	if err != nil {
		return shim.Error(fmt.Sprintf("failed to sign the updated document: %v", err))
	}
	/* End research project part */

	// Prepare the updated signed document to be stored on the shared ledger
	updatedAssetJSON, err := json.Marshal(updatedDoc)
	if err != nil {
		return shim.Error(fmt.Sprintf("failed to marshal document: %v", err))
	}

	// Store the updated signed document on the shared ledger
	er := stub.PutState(assetID, updatedAssetJSON)
	if er != nil {
		return shim.Error(fmt.Sprintf("failed to write the document to the ledger: %v", er))
	}

	return shim.Success(nil)
}

// TransferAsset transfers an asset from the caller's organization to the buyer's organization
func (s *SmartContract) TransferAsset(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	assetID := args[0]
	buyerOrgID := args[1]

	// Read an asset with the provided assetID from the shared ledger
	doc, err := s.ReadDocument(stub, assetID)
	if err != nil {
		return shim.Error(fmt.Sprintf("failed to get asset: %v", err))
	}

	/* Begin research project part */
	// Check if the signature of the signed document is valid
	tamperedWith, err := doc.verifyDoc_onChain()
	if err != nil {
		shim.Error(fmt.Sprintf("failed to verify the asset: %v", err))
	}

	if tamperedWith {
		return shim.Error(fmt.Sprintf("asset with id %v has been tampered with", assetID))
	} else {
		doc.Asset.OwnerOrg = buyerOrgID
	}

	// Instantiate a connection with the tpm to sign assets
	rwc, closer, err := GetTPM()
	if err != nil {
		return shim.Error(fmt.Sprintf("failed to connect to the TPM: %v", err))
	}
	defer closer() // Closes the tpm at the end of usage

	// Signed
	updatedDoc, err := doc.Asset.signAsset_onChain(rwc)
	if err != nil {
		return shim.Error(fmt.Sprintf("failed to sign the updated document: %v", err))
	}
	/* End research project part */

	// Prepare the updated signed document to be stored on the shared ledger
	updatedAssetJSON, err := json.Marshal(updatedDoc)
	if err != nil {
		return shim.Error(fmt.Sprintf("failed to marshal asset: %v", err))
	}

	// Store the updated signed document on the shared ledger
	err = stub.PutState(doc.Asset.ID, updatedAssetJSON)
	if err != nil {
		return shim.Error(fmt.Sprintf("failed to write asset for buyer: %v", err))
	}

	return shim.Success(nil)
}

// ReadAsset reads the document information from the shared ledger
func (s *SmartContract) ReadDocument(stub shim.ChaincodeStubInterface, documentID string) (*SignedDocument, error) {
	// Read a document with the provided id from the shared ledger
	docJSON, err := stub.GetState(documentID)
	if err != nil {
		return nil, fmt.Errorf("failed to read from the shared ledger: %v", err)
	}
	if docJSON == nil {
		return nil, fmt.Errorf("%s does not exist", documentID)
	}

	// Convert JSON encoded document into a signed document struct
	var doc *SignedDocument
	err = json.Unmarshal(docJSON, &doc)
	if err != nil {
		return nil, err
	}

	return doc, nil
}

// Start a chaincode server
func main() {
	// This is the chaincode id from the already installed chaincode
	ccid := "mycc:724bf2be51e5a9e98d79d15d482d4fb1666af022c7f1368de18dec355d839da8"

	// Start chaincode server on localhost port 7075
	server := &shim.ChaincodeServer{
		CCID:    ccid,
		Address: "localhost:7075",
		CC:      new(SmartContract),
		TLSProps: shim.TLSProperties{
			Disabled: true,
		},
	}
	fmt.Printf("Starting chaincode %s at %s\n", server.CCID, server.Address)
	err := server.Start()

	if err != nil {
		fmt.Printf("Error starting Simple chaincode: %s", err)
	}
}
